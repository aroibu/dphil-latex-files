\chapter{\label{ch:2-materials}General Materials and Methods} 

\minitoc

\section{Datasets}

\subsection{Acquisition}

Neuroimaging data for \(22,000\) subjects aged 40-69 was randomly selected from the UK Biobank, an epidemiological database acquiring scans across multiple sites using identical hardware, software, standardised protocols (Table \ref{tab:my-table}), processing and quality control protocols \cite{miller2016multimodal, alfaro2018image, smith2020brain}. The dataset size ensures sufficiently large biological variability. Following acquisition, the data was anonymised and had 3D GDC applied. All data processing was carried out using FSL 5.0 and the FBP approach \cite{jenkinson2012fsl, smith2004advances, alfaro2018image}, files being saved in a NIFTI format. The following sections describing the dMRI and rsfMRI data come from the paper by Fidel Alfaro-Almagro et al. (2018) \cite{alfaro2018image}.

\begin{table}[]
\centering
\caption{\textbf{UK Biobank MRI Acquisition Protocol} (Source: \cite{alfaro2018image})}
\label{tab:my-table}
\resizebox{\textwidth}{!}{%
\begin{tabular}{cccclc}
\hline
\textbf{Modality} & \begin{tabular}[c]{@{}c@{}}\textbf{Duration}\\ \textbf{(min)}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{Voxel}\\ \textbf{(mm)}\end{tabular} & \textbf{Matrix}          & \multicolumn{1}{c}{\textbf{Key Parameters}}                                                                                                                                                                                                                                                                                                                       & \textbf{Volumes / Timepoints}                               \\ \hline \hline
T1       & 4:54                                                     & 1.0 x 1.0 x 1.0                                      & 208 x 256 x 256 & \begin{tabular}[c]{@{}l@{}}3D MPRAGE, sagittal, R = 2\\ TI/TR = 880/ 2000 ms\end{tabular}                                                                                                                                                                                                                                                                & 1                                                            \\ \hline
dMRI     & 7:08                                                     & 2.0 x 2.0 x 2.0                                      & 104 x 104 x 72  & \begin{tabular}[c]{@{}l@{}}MB = 3, R = 1, TE/ TR = 92/3600 ms\\ PF 6/8, fat sat, b = 0 s=mm\textsuperscript{2}\\ (\(5x + 3\times\) phase-encoding reversed)\\ b = 1 000 s=mm\textsuperscript{2} (50\(\times\)), \\ b = 2000 s=mm\textsuperscript{2} (50\(\times\))\end{tabular} & \begin{tabular}[c]{@{}c@{}}105 + 6 \\ (AP + PA)\end{tabular} \\ \hline
rsfMRI   & 6:10                                                     & 2.4 x 2.4 x 2.4                                      & 88 x 88 x 64    & \begin{tabular}[c]{@{}l@{}}TE/TR = 39/735 ms, MB = 8, \\ R = 1, flip angle 52\textdegree, fat sat\end{tabular}                                                                                                                                                                                                                                & 490                                                          \\ \hline
\end{tabular}%
}
\end{table}




\subsection{dMRI Data}

dMRI data tracks water molecule diffusion speeds, estimating both voxel-level microstructural properties and macroscopic connectivity pathways through long-range tractography \cite{beaulieu2002basis, miller2016multimodal}. AP data was acquired across 50 directions, while PA data was acquired for geometric distortion correction. AP data was correct for eddy currents and head motion using the EDDY tool \cite{andersson2015non, andersson2016incorporating, andersson2016integrated}, after which GDC was applied. DTIs were then fitted using DTIFIT \cite{basser1994estimation} with the resulting FA image being aligned to the 1mm MNI152 standard space white-matter skeleton with TBSS \cite{smith2006tract}. TBSS alignment is deterministic, as it first uses FLIRT \cite{jenkinson2002improved} for full affine linear alignment and then high-dimensional FNIRT \cite{andersson2007non}. 

Long-range tracts were estimated using tractography-based analysis. First, per-voxel 3-fiber orientations were estimated, after which probabilistic tractography with crossing fiber modelling using PROBTRACKX \cite{behrens2003characterization, behrens2007probabilistic} automatically mapped 27 major tracts using standard-space AutoPTX defined ROIs (\cite{de2013improving}). Seed numbers per voxel were set to ensure consistency by preventing median correlations between same-subject tractography runs from falling under 0.999. This generated streamline density maps, showing the probability of a streamline from a seed region traversing each voxel, which were registered in 1mm standard space using the TBSS generated warps. As the degree of overlap between the 27 major tracts was small, they were summed voxel-wise.

\subsection{rsfMRI}

rsfMRI measures hemodynamic activity induced by neural metabolic demand, providing an indirect indication of neural activity in the absence of task \cite{suarez2020linking, miller2016multimodal, logothetis2008we}. The data was GDC warped and fed into MELODIC \cite{beckmann2004probabilistic} together with warps of T1 to the MNI152 space and several other T1 modalities. MELODIC performed EPI and GDC unwarping, motion correction, intensity normalisation and highpass temporal filtering. Structured artefacts were removed using ICA-FIX \cite{salimi2014automatic, griffanti2014ica}. The MELODIC internal registration breaks the dMRI-rsfMRI correspondence, yet, this can be mediated through an intermediary medium such as a NN. MIGP group-PCA then identified the top 1200 components for the RSNs of 4100 subjects, which where fed into ICA using MELODIC, facilitating the generation of 25-dimensional ICA maps. These are overlapping, continuous parcellations of the cortical and sub-cortical gray matter. After discarding artefact components, the remaining 21 ICA maps were dual regressed \cite{beckmann2009group, nickerson2017using} to generate subject-specific spatial maps returned as z-stats. For each subject, the group-average spatial map was regressed into the subject's 4D data, with the resulting subject-specific timeseries being regressed again into the 4D data.

\subsection{Pre-processing}\label{sec:pre-processing}

Before network training, several additional processing steps were applied. Firstly, to reduce computational requirements and ensure dimensional compatibility between dMRI and rsfMRI data, the dMRI inputs were smoothed using a 1D Gaussian convolutional kernel \cite{FSLpy} and down-sampled to 2mm-spacial resolution. Both inputs and outputs where further cropped to \(80 \times 96 \times 80\) voxels \cite{abramian2019generating}. Out of the 21 functional sub-divisions, only the DMN forms the targets of this project, with previous studies finding it to be active during rest and that regions comprising it are structurally linked and highly connected, suggesting the existence of dense anatomical links \cite{honey2009predicting, greicius2009resting, honey2010can}.

Experiments were run on both original and residualised datasets. To constraining the network in learning better mappings of individual particularities, the population means calculated across \(38,817\) subjects were regressed from both targets and inputs (\ref{eq1}-\ref{eq2}).

\begin{equation} \label{eq1}
\pmb{V_u} = w \times \pmb{M} + \pmb{V_r}
\end{equation}

\begin{equation} \label{eq2}
w = \left( \pmb{M}^\top \pmb{M} \right)^{-1} \pmb{M}^\top \pmb{V_u}
\end{equation}

Data was scaled between \([-1, 1]\) to stabilise learning and prevent exploding gradients, by dividing by a scaling value. Finding the scaling values is nontrivial due to the nature of the data. Thus, a data study was done across all Biobank subjects with the scaling values being chosen as the largest absolute value between the extreme value mean plus or minus two standard deviations (Figures \ref{fig:OrigDataStudy}-\ref{fig:RegDataStudy}). Voxel intensities outside these parameters were saturated to them. This ensured sufficient data was retained while outliers were eliminated. The datasets are also highly sparse, with higher intensities capturing individual particularities accounting  \(\leq2\%\) of voxels. Following scaling, the \(22,000\) subjects where randomly split into train, validation and test datasets using the \(90-5-5\) \% rule. Each subjects consists of input-dMRI and target-rsfMRI data-pairs.

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dMRI_orig_Voxel_Intensity_Value_Distribution__Bar.png}
        \caption{dMRI voxel intensity distribution}%
        \label{fig:barDMRIorig}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_orig_Voxel_Intensity_Value_Distribution__Bar.png}
        \caption{rsfMRI voxel intensity distribution}%
        \label{fig:barFMRI}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dMRI_orig_Min_Value_Distribution_min_Hist.png}
        \caption{dMRI min voxel intensity distribution}%
        \label{fig:minDMRI}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dMRI_orig_Max_Value_Distribution_max_Hist.png}
        \caption{dMRI max voxel intensity distribution}%
        \label{fig:maxDMRI}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_orig_Min_Value_Distribution_min_Hist.png}
        \caption{rsfMRI min voxel intensity distribution}%
        \label{fig:minFMRI}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_orig_Max_Value_Distribution_max_Hist.png}
        \caption{rsfMRI max voxel intensity distribution}%
        \label{fig:maxFMRI}
    \end{subfigure}
    
    \caption[Voxel intensity value distributions for original data across the Biobank]{{\bf Voxel intensity value distributions for original data across the Biobank.} Voxel intensity distributions at discrete percentile points for the dMRI \textbf{(a)} and rsfMRI \textbf{(b)} data display a high degree of sparsity and noise. Min-Max distributions for dMRI \textbf{(c)}-\textbf{(d)} and rsfMRI \textbf{(e)}-\textbf{(f)} are non-trivial, leading to scaling values (dashed red lines) being selected by adding two standard deviation to the mean max value, and subtracting them from the mean min value.}
    \label{fig:OrigDataStudy}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dMRI_reg_Voxel_Intensity_Value_Distribution__Bar.png}
        \caption{dMRI voxel intensity distribution}%
        \label{fig:barDMRIreg}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_reg_Voxel_Intensity_Value_Distribution__Bar.png}
        \caption{rsfMRI voxel intensity distribution}%
        \label{fig:barFMRIreg}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dMRI_reg_Min_Value_Distribution_min_Hist.png}
        \caption{dMRI min voxel intensity distribution}%
        \label{fig:minDMRIreg}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dMRI_reg_Max_Value_Distribution_max_Hist.png}
        \caption{dMRI max voxel intensity distribution}%
        \label{fig:maxDMRIreg}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_Min_Value_Distribution_min_Hist.png}
        \caption{rsfMRI min voxel intensity distribution}%
        \label{fig:minFMRIreg}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/rsfMRI_Max_Value_Distribution_max_Hist.png}
        \caption{rsfMRI max voxel intensity distribution}%
        \label{fig:maxFMRIreg}
    \end{subfigure}
    
    \caption[Voxel intensity value distributions for residualised data across the Biobank]{{\bf Voxel intensity value distributions for residualised data across the Biobank.} Voxel intensity distributions at discrete percentile points for the dMRI \textbf{(a)} and rsfMRI \textbf{(b)} data. Min-Max distributions for dMRI \textbf{(c)}-\textbf{(d)} and rsfMRI \textbf{(e)}-\textbf{(f)} and scaling values (dashed red lines).}
    \label{fig:RegDataStudy}
\end{figure}

\section{Linear Models}

Established linear models were used as controls in this study. The truncated SVD algorithm was utilised for assessing reconstruction and feature retention after dimensionality reduction, while the PLS algorithm was the baseline for translating between domains.

\subsection{SVD}

The compact SVD operation factorises a real matrix \(\pmb{A}\) (\ref{eq3}) by changing the domain and codomain vector spaces' basis, enabling the isolation of orthogonal vectors best describing the matrix. By retaining only the top \(k\) largest singular values and centering \(\pmb{A}\) by subtracting the column-wise feature means \(\overline{\pmb{A}}\), (\ref{eq4}) becomes equivalent to PCA. Transforming a test matrix \(\pmb{A}_{test}\) is achieved with (\ref{eq5}).

\begin{equation} \label{eq3}
% \begin{aligned}
\pmb{A}^{m \times n} = \pmb{U}^{m \times r} \pmb{\Sigma}^{r \times r} {\pmb{V}^{T}}^{r \times n}, 
r = min\left(m, n\right)
% \end{aligned}
\end{equation}

\begin{equation} \label{eq4}
\pmb{A}\approx \pmb{A}_{k} = \pmb{U}_{k}\pmb{\Sigma} _{k}\pmb{V}_{k}^{T}
\end{equation}

\begin{equation} \label{eq5}
\pmb{A}_{test}\approx \widehat{\pmb{A}}_{test} = \left[ \left( \pmb{A}_{test}-\overline{\pmb{A}}\right) \pmb{V}_{k}\right] \pmb{V}_{k}^{T}+\overline{\pmb{A}}
\end{equation}

\subsection{PLS}

By modelling the covariance structures in two real matrices \(\pmb{A}\) and \(\pmb{B}\), PLS finds \(K\) multidimensional directions in \(\pmb{A}\) space that explains the maximum variance direction in the \(\pmb{B}\) space \cite{scikit-learn}. It does this by decomposing them into scores (\(\pmb{\Xi}\),\(\pmb{\Omega}\)) and loadings (\(\pmb{\Gamma}\),\(\pmb{\Delta}\)) matrices (\ref{eq6}). For each \(k\in K\), scores are projections of \(\pmb{A}_k\) and \(\pmb{B}_k\) on the singular vectors of the cross-covariance matrix (\ref{eq7}), while loadings are the regressions of \(\pmb{A}_k\) and \(\pmb{B}_k\) on the scores. For each \(k\), the matrices are deflated by subtracting the rank-1 approximations created by corresponding scores and loadings. Predicting targets (\ref{eq8}) requires \(\pmb{\Upsilon}\), the matrix with left singular values of \(\pmb{C}\). \(\pmb{A}\) and \(\pmb{B}\) are transformed prior to PLS by multiplication with \(\pmb{V}\) from (\ref{eq4}).

\begin{equation} \label{eq6}
% \begin{aligned}
\pmb{A}^{n \times d} = \pmb{\Xi}^{n \times K} \pmb{\Gamma}^{K \times d} \mbox{, } \pmb{B}^{n \times t} = \pmb{\Omega}^{n \times K} \pmb{\Delta}^{K \times t}
\end{equation}

\begin{equation} \label{eq7}
\pmb{C} = \pmb{A}_k^T \pmb{B}_k
\end{equation}

\begin{equation} \label{eq8}
\widehat{\pmb{B}} = \pmb{\Omega}\pmb{\Delta}^T = \alpha\pmb{\Xi\Delta}^T = \alpha \pmb{A P \Delta}^T \mbox{ where } \pmb{\Omega}=\alpha\pmb{\Xi} \mbox{, } \pmb{P} = \pmb{\Upsilon}(\pmb{\Gamma}^T \pmb{\Upsilon})^{-1}
\end{equation}

\section{Deep Learning Blocks}

\subsection{Supervised Learning}

Given an individual's SC input \(\pmb{x}\), this study aims at learning their FC mapping \(\pmb{y}\) by learning a function \(f_m:\mathcal{X} \mapsto \mathcal{Y}\) expressed as a DNN, given a batch of input structural data \(\mathcal{X}= \{\pmb{x}_1, \pmb{x
_2},..., \pmb{x}_N\} \) and corresponding target functional data \(\mathcal{Y}= \{\pmb{y}_1, \pmb{y}_2,..., \pmb{y}_N \} \). DNNs are composed of a series of stacked layers \(l \in L\) containing learnable weights \(\pmb{w}_l\in \mathcal{W}\) and bias terms \(b_l\) which extract information from the data. These need to be optimised for the given task by minimising a cost \(\mathcal{J}\) based on a loss function \(\mathcal{L}\) (\ref{eq9}). The MSE loss (where \(n\) is the total number of elements) (\ref{eq9B}) minimises the reconstruction error between the predictions and targets, and was used in this study. 

\begin{equation} \label{eq9}
\mathcal{J}=\dfrac{1}{N}\sum ^{n}_{i=1}\mathcal{L}\left( \pmb{y}_i,f_m\left( \pmb{x}_i\right) \right) 
\end{equation}

\begin{equation} \label{eq9B}
\mathcal{L} = \frac{1}{n}\sqrt{\sum_{i=1}^{n}(\pmb{y}_{i} - f_m\left( \pmb{x}_i\right))^{2}}
\end{equation}

The proposed DL architecture follows an end-to-end, fully-connected, encoder-transformer-decoder configuration. The encoder down-samples the input data, learning low-level features, the transformer learns the desired underlying relationship, and the decoder recovers the transformed encoded information and constructs a new data representation. 

\subsection{Fully Connected Layers}

Linear, or fully connected layers simple DL operations, sitting at the core of most methods. Each layer is formed of a series of neurons composed of weights and biases \( \{\pmb{w}_l^{(k)}, b_l^{(k)} \} \) which compute an affine transformation \ref{eq10} for an input activation map \( \pmb{z}_l \mbox{ where } \pmb{z}_0 = \pmb{x} \) \cite{goodfellow2016deep}.

\begin{equation} \label{eq10}
f_m\left(\pmb{z}_l, \mathcal{W}_l \right) = \pmb{z}_{l-1} \ast \pmb{w}_l^{(k)} + b_l^{(k)} \mbox{ where } \mathcal{W}_l = ( \pmb{w}_l,  b_l )
\end{equation}

\subsection{Convolutional Layers}

The training data consists of 3D voxel intensities matrices converted to tensors. Convolutions extract information from 3D tensor data \(\pmb{I}\) with a grid-like structure through cross-correlation by using a kernel filter composed of multiple weights \(\pmb{k}\) and a bias \(\pmb{b}\) (\ref{eq11}) \cite{goodfellow2016deep, pytorch}. Zero-padding layers are added before convolutions to prevent information loss in border regions. The padding size is calculated using (\ref{eq12}), where \(S\) represents the dimension in a direction. A convolution operation is composed of multiple kernels, each producing an equal amount of output channels, aiding in capturing multiple features \cite{kaji2019overview}. Kernels shift by various steps known as strides. Transpose convolutions represent the gradient of traditional convolutions with respect to their input, having a similar sequence of operations.

\begin{equation} \label{eq11}
(\pmb{k} \ast \pmb{I})(i,j,k) = \pmb{b}(m,n,p) + \sum_{m}\sum_{n}\sum_{p}\pmb{I}(i-m, j-n, k-p)\pmb{k}(m,n,p)
\end{equation}

\begin{equation} \label{eq12}
S_{pad} = int\left(\frac{S_{kernel}-1}{2}\right)
\end{equation}

\subsection{Normalisation Layers}

Multiple 3D tensors can be fed simultaneously in batches. As batch sizes impact the training performance of models, normalizing techniques such as Batch Normalisation are applied to improve performance by preserving features which are more discriminative \cite{ioffe2015batch, goodfellow2016deep}. In (\ref{eq13}), \(\pmb{x}\) and \(\pmb{y}\) are the input and output tensors, \(\gamma\) and \(\beta\) are learnable parameters scaling and shifting the data, \(\epsilon=1e-5\) is constant, \(\mu\) is the mean and \(\sigma^2\) the variance of the batch. Training estimates of compounded mean and variances are kept and used during evaluation. A \(momentum\) term equal to \(0.1\) is applied to help reduce noise (\ref{eq14}), for an estimated statistic \(\hat{s}\) and \(s_t\) new observed value \cite{pytorch}.

\begin{equation} \label{eq13}
\pmb{y} = \frac{\pmb{x} - \mu[\pmb{x}]}{ \sqrt{\sigma^2[\pmb{x}] + \epsilon}} * \gamma + \beta
\end{equation}

\begin{equation} \label{eq14}
\hat{s}_{new} = (1-momentum) \times \hat{s} + momentum \times s_t
\end{equation}

\subsection{Non-Linear Activations}

Non-linear activations, such as Tanh (\ref{eq15} and PReLU \cite{he2015delving} \ref{eq16} enable the learning of complex features. PReLU generalises the ReLU and LeakyReLU functions, improving model fitting by learning a parameter \(a\) across all input channels, allowing kernels to output negative values and avoiding zero gradients. 

\begin{equation} \label{eq15}
        \text{Tanh}(x) = \frac{\exp(x) - \exp(-x)} {\exp(x) + \exp(-x)}
\end{equation}

\begin{equation} \label{eq16}
        \text{PReLU}(x) =
        \begin{cases}
        x, & \text{ if } x \geq 0 \\
        ax, & \text{ otherwise }
        \end{cases}
\end{equation}

\subsection{Weight Initialisation}

Trainable network parameters were initialized using the Kaiming He initialization, proven to perform best when using ReLU derived activation functions (\ref{eq17}) \cite{he2015delving}, where layer \(l\)'s weights \(\mathcal{W}\) are sampled from a uniform distribution \(\mathcal{U}\) based on the input dimensionality \(n_l\).

\begin{equation} \label{eq17}
\mathcal{W}_l \sim \mathcal{U} \left[ - \frac{\sqrt{6}}{\sqrt{n_l + n_{l+1}}}, \frac{\sqrt{6}}{\sqrt{n_l + n_{l+1}}} \right]
\end{equation}

\subsection{Network Training}

The network is trained using gradient descent with the ADAM optimization algorithm \cite{kingma2014adam}, which computes the decaying averages of the first and second moments \(m_t\) and \(v_t\) at step \(t\). These are bias corrected, producing \(\hat{m}_t\) and \(\hat{v}_t\), which are used to update the trainable parameters \(\theta_t\) (\ref{eq18})-(\ref{eq22}). The \(\beta_1,\beta_2,\) and \(\epsilon\) constants were initialised to their default values \cite{kingma2014adam}, while the weight decay \(\lambda\) was initialised to \(1e-5\) \cite{roy2019quicknat}. The learning rate was chosen after experimentation to \(\eta=1e-3\) and multiplied by \(0.5\) if the validation loss failed to decrease for \(15\) training epochs. If the validation loss failed to decrease after \(30\) epochs, training was stopped and the model was saved with the best performing parameters. 

\begin{equation} \label{eq18}
m_t = \beta_1 m_{t-1} + (1-\beta_1)d\theta_t
\end{equation}
\begin{equation} \label{eq19}
v_t = \beta_2 v_{t-1} + (1-\beta_2)d\theta_t^2
\end{equation}
\begin{equation} \label{eq20}
\hat{m}_t = \frac{m_t}{1 - {\beta_1}^t}
\end{equation}
\begin{equation} \label{eq21}
\hat{v}_t = \frac{v_t}{1 - {\beta_2}^t}
\end{equation}
\begin{equation} \label{eq22}
\theta_{t+1} = \theta_t - \eta \left( \frac{\hat{m}_t}{\sqrt{\hat{v}_t} + \epsilon} + \lambda\theta_t \right)
\end{equation}

