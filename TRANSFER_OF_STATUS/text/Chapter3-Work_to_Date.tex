\chapter{\label{ch:3-work}Work To Date} 

\minitoc

Given the complexity involved by the rich between-modality modelling, a systematic approach was employed in developing a deep learning model capable of tackling it. Firstly, two established architectures, a U-Net and FCN, were investigated, producing encouraging results, yet not succeeding in capturing individual particularities. This prompted the development of a new architecture, which was methodically tested by first assessing it's capacity to extract, encode and recover information from each modality, and then investigating it's power to translate between disjoint modalities. The following presents either generally observed results or results obtained with the best performing models.

\section{Established Architectures: U-Net and FCN}

Depending on their nature, datasets used for domain-to-domain translation can either be paired, when they are different in appearance but render the same basal information, or unpaired, where there is some not immediately visible underlying relationship between the domains that the network has to learn.

\subsection{U-Net}

Assuming the datasets are paired, as the input-output pairs are subjects-specific, the first studied architecture was a U-Net \cite{ronneberger2015u}. This model is popular in numerous neuroimaging applications \cite{dewey2019deepharmony, pham2019multiscale, ghodrati2019mr, roy2019quicknat, henschel2019fastsurfer} as besides it's encoder-decoder FCN architecture, it employs skip-connections which concatenate features from the encoder to the decoder path to recover information which might be lost during down-sampling. A 3D U-Net \cite{cciccek20163d} (Figure \ref{fig:3DUNet}) was employed.

Despite parameter and architecture tuning, in the case of both original and residualised data, the U-Net either predicted results closely resembling the mean DMN population map, or made predictions which included patterns resembling the input structural information (Figure \ref{fig:3DUNet_predictions} due to the concatenation of similarly scaled tract and DMN features. 

\begin{figure}[!hbt]
\begin{center}
\includegraphics[width=0.999\textwidth]{figures/simple3DUNet_report.png}
\caption[3D U-Net Architecture]{{\bf 3D U-Net Architecture.} The vertical values indicate the shape of the data at each layer in the network, while the horizontal values represent the number of features.}
\label{fig:3DUNet}
\end{center}
\end{figure}

\begin{figure}[!hbt]
    \centering
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/learnedTracts.png}
        \caption{Example showing network predicting tracts as functional activation sites.}%
        \label{fig:learnedTracts}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/learnedPopMean.png}
        \caption{Example showing network predicting population mean activation sites}%
        \label{fig:learnedPopMean}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/groupmean.png}
        \caption{Mean functional connectivity across the UK Biobank}%
        \label{fig:groupmean}
    \end{subfigure}
    \caption[Typical 3D U-Net Predictions]{{\bf Typical 3D U-Net Predictions.} Blue is the ground truth while red is the prediction. Depending on the employed network parameters, the 3D U-Net either made \textbf{(a)} predictions which included tract information or \textbf{(b)} mean functional activation, even for residualised data where the \textbf{(c)} mean functional map was regressed from individual subjects}
    \label{fig:3DUNet_predictions}
\end{figure}

\subsection{Fully Convolutional Network}

Following these observations, the hypothesis regarding the nature of the data was changed to unpaired, as although data pairs are subject-specific, the two modalities encode substantially different information types. The architecture was thus changed to the generator used in the cycleGAN model \cite{CycleGAN2017}, which follows a FCN encoder-transformer-decoder approach but lacks skip-connections (Figure \ref{fig:3DFCN}). The architecture has been successfully used for style-transfer and super-resolution \cite{radford2015unsupervised, johnson2016perceptual}, but also in domain-to-domain translation between T1 and T2 sMRI \cite{welander2018generative}, dMRI scalar maps \cite{gu2019generating} and fMRI volumes \cite{abramian2019generating} when in the CycleGAN configuration.

Yet, experiments with this model continued predicting a form of the DMN population mean (Figure \ref{fig:3DFCN_predictions}. The fact that this also occurs for residualised data raised questions regarding the translation and sentencing capability of a FCN network, but also about how much relevant information can be extracted from the modalities.

\begin{figure}[!hbt]
\begin{center}
\includegraphics[width=0.999\textwidth]{figures/3d_AE_Network_2.png}
\caption[3D FCN Architecture]{{\bf 3D FCN Architecture.} The vertical values indicate the shape of the data at each layer in the network, while the horizontal values represent the number of features.}
\label{fig:3DFCN}
\end{center}
\end{figure}

\begin{figure}[!hbt]
\begin{center}
\includegraphics[width=0.65\textwidth]{figures/VA4-1_report.png}
\caption[Typical 3D FCN Prediction]{{\bf Typical 3D FCN Prediction} which continues to resemble the population mean DMN map from Figure \ref{fig:groupmean}. Blue is the ground truth while red is the prediction.}
\label{fig:3DFCN_predictions}
\end{center}
\end{figure}

\section{Representation Learning}

To evaluate both how much relevant information is contained in the two modalities and an architecture's capacity to reduce the data dimensionality to promote relevant feature learning, a new AE architecture (Figure \ref{fig:3DAE}) based on the successful VGG-16 network \cite{simonyan2014very} was proposed. Compared to a FCN, the new architecture unravelled the 3D data after the encoder path and employed two FC layers to both reduce the latent dimensionality and establish relationships between spatially distant features. This is particularly relevant for between-domain translation, where functional correlations occur between regions which are several synapses removed.

\begin{figure}[!hbt]
\begin{center}
\includegraphics[width=0.999\textwidth]{figures/autoecoder_architecture.png}
\caption[3D AE Architecture]{{\bf 3D AE Architecture.} The vertical values indicate the shape of the data at each layer in the network, while the horizontal values represent the number of features.}
\label{fig:3DAE}
\end{center}
\end{figure}

Following an architecture and hyperparameter refinement study, the AE's dimensionality reduction and reconstruction capabilities were compared against PCA models with equivalent latent space dimensionality (Figure \ref{fig:AEvsSVD}), with the AEs performing comparable or better than the equivalent PCA models. The AE performance can be seen dropping for dMRI data between \(1024-2048\) latent elements, where it starts overfitting to noise (Figure \ref{fig:AppVariance}). Visually inspected data reconstructions (Figure \ref{fig:AE_Reconstructions}) reveal that for the proposed architecture is capable of capturing and reconstructing relevant features despite the sparsity of the data. The training curves in Figure \ref{fig:App3DAE_training} provide an indication of the maximum achievable performance in terms of translation reconstruction.

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/AE4_SVD_error_bar_comparison_MSE.png}
        \caption{dMRI MSE}%
        \label{fig:AE4_SVD_error_bar_comparison_MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/AE4_SVD_error_bar_comparison_pearsonR_absolute.png}
        \caption{dMRI Correlation}%
        \label{fig:AE4_SVD_error_bar_comparison_pearsonR_absolute}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/AE5_SVD_error_bar_comparison_MSE_abs.png}
        \caption{rsfMRI MSE}%
        \label{fig:SVD_AE_dmri_regressed_1024MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/AE5_SVD_error_bar_comparison_pearsonR_absolute}
        \caption{rsfMRI Correlation}%
        \label{fig:SVD_AE_dmri_regressed_1024MSE}
    \end{subfigure}
    \caption[3D AE vs. SVD Reconstructiond]{{\bf 3D AE vs. SVD Reconstruction} for residualised dMRI and rsfMRI data at various latent dimensionalities. A similar study was conducted for non-residualised data.}
    \label{fig:AEvsSVD}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/SAT1-3_P.png}
        \caption{dMRI Reconstruction}%
        \label{fig:SAT1-3_GT}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/SAT1-3_GT.png}
        \caption{dMRI Grount Truth}%
        \label{fig:AE4_SVD_error_bar_comparison_pearsonR_absolute}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/SAF1-3_P.png}
        \caption{rsfMRI Reconstruction}%
        \label{fig:SAT1-3_GT}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/SAF1-3_GT.png}
        \caption{rsfMRI Grount Truth}%
        \label{fig:AE4_SVD_error_bar_comparison_pearsonR_absolute}
    \end{subfigure}
    \caption[3D AE Reconstructions]{{\bf 3D AE Reconstructions} for residualised data at \(K=1024\) latent elements. Red structures represents positive correlated data, while blue represents negative anti-correlated data. Images were hard shrunk to eliminate background noise.}
    \label{fig:AE_Reconstructions}
\end{figure}

\section{Cross-Domain Translation}

\subsection{Disjoint Modality Translation}

Building on these encouraging results, the architecture was modified by increasing the depth of the FC transformer by adding more FMLs (Figure \ref{fig:E2EAE}). Before translating directly between structure and function, the network's ability to translate between disjoint information in the WM and GM was assessed by using tract data as inputs and GM VBM data as targets. VBM represents an unbiased technique for characterising regional cerebral volume and voxel-wise tissue concentration differences in local GM \cite{douaud2007anatomically}. A recent study \cite{gong2021phenotype} found strong cosine similarities between the two modalities in terms of their contribution to multimodal decomposition, suggesting stronger inter-modality relationships than between structure and resting data (Figure \ref{fig:app_weikang}). A follow-up investigation of double-scanned, both residualised and non-residualised Biobank subject data found stronger correlations in tract and VBM data than for the resting-state modalities (Figure \ref{fig:bb_repeat}), owing to factors such as the lower SNR of resting data but also subject fatigue or scanning procedure non-compliance. VBM data processing was done similarly to the dMRI pre-processing described in Section \ref{sec:pre-processing} and Figure \ref{fig:appVBMDataStudy}, with the difference that the Gaussian filter kernel with \(FWMH=5mm\) being used in accordance with \cite{gong2021phenotype}.

\begin{figure}[!hbt]
\begin{center}
\includegraphics[width=0.999\textwidth]{figures/end2end_autoencoder_architecture.png}
\caption[3D End-to-End Architecture]{{\bf 3D End-to-End Architecture.} The vertical values indicate the shape of the data at each layer in the network, while the horizontal values represent the number of features.}
\label{fig:E2EAE}
\end{center}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/bb_repeat_original_diagonal_BAR.png}
        \caption{Original Diagonal}%
        \label{fig:bb_repeat_original_diagonal_BAR}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/bb_repeat_original_off_diagonal_BAR.png}
        \caption{Original Off-Diagonal}%
        \label{fig:bb_repeat_original_off_diagonal_BAR}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/bb_repeat_regressed_diagonal_BAR.png}
        \caption{Residualised Diagonal}%
        \label{fig:bb_repeat_regressed_diagonal_BAR}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/bb_repeat_regressed_off_diagonal_BAR.png}
        \caption{Residualised Off-Diagonal}%
        \label{fig:bb_repeat_regressed_off_diagonal_BAR}
    \end{subfigure}

    \caption[Biobank Repeated Scan Correlations]{{\bf Biobank Repeated Scan Correlations}}

    \label{fig:bb_repeat}
\end{figure}

In parallel with the networks trained to translate between dMRI and VBM data, with \(K=1024\) latent dimensions and \(2\) FC FMLs (Figure \ref{fig:VBM_training}), a sensitivity study was conducted to find the best PLS model conducing a similar operation for both original and residualised data (Figure \ref{fig:PLS_VBM_refinement}). For both datasets, the DL models performed better than the corresponding PLS models (Figure \ref{fig:Translation_VBM_Performance}), successfully capturing discriminative features between subjects while not learning a version of the population mean (Figure \ref{fig:VBM_Reconstructions}) thus proving the proposed model's ability for between disjoint modality translation.

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/vbm_e2e_orig_mse_train_curves.png}
        \caption{Original MSE}%
        \label{fig:vbm_e2e_orig_mse_train_curves}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/vbm_e2e_orig_pearsomn_train_curves.png}
        \caption{Original Correlation}%
        \label{fig:vbm_e2e_orig_pearsomn_train_curves}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/vbm_e2e_mse_train.png}
        \caption{Residualised MSE Validation Curves}%
        \label{fig:vbm_e2e_conv_cd_comparison_mse}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/vbm_e2e_pearson_train.png}
        \caption{Residualised Correlation Validation Curves}%
        \label{fig:vbm_e2e_conv_cd_comparison_pearson}
    \end{subfigure}
    \caption[VBM Translation Training Curves]{{\bf VBM Translation Training Curves.}}
    \label{fig:VBM_training}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/pls_vbm_original_sensitivity_mse.png}
        \caption{Original MSE}%
        \label{fig:pls_vbm_original_sensitivity_mse}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_vbm_original_sensitivity_pearson.png}
        \caption{Original Correlation}%
        \label{fig:pls_vbm_original_sensitivity_pearson}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_vbm_regressed_sensitivity_mse.png}
        \caption{Residualised MSE}%
        \label{fig:pls_vbm_regressed_sensitivity_mse}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_vbm_regressed_sensitivity_pearson}
        \caption{Residualised Correlation}%
        \label{fig:pls_vbm_regressed_sensitivity_pearson}
    \end{subfigure}
    \caption[PLS Refinement Study for VBM Data]{{\bf PLS Refinement Study for VBM Data} for various K-top PCA components and across multiple PCA components. The dashed red line represents the performance of the best DL models. The best original data model used 4096 PCA and 345 PLS components, while the best residualised used 4096 PCA and 326 PLS components, suggesting that smaller latent dimensionalities could be attempted for the DL networks.}
    \label{fig:PLS_VBM_refinement}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/VBM_original_MSE.png}
        \caption{Original MSE}%
        \label{fig:VBM_original_MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/VBM_original_MSE.pngPearson R.png}
        \caption{Original Correlation}%
        \label{fig:VBM_original_P}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/VBM_regressed_MSE.png}
        \caption{Residualised MSE}%
        \label{fig:VBM_reg_MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/VBM_regressed_MSE.pngPearson R}
        \caption{Residualised Correlation}%
        \label{fig:VBM_reg_P}
    \end{subfigure}
    \caption[DL vs. PLS Translation Performance for VBM Data]{{\bf DL vs. PLS Translation Performance for VBM Data}}
    \label{fig:Translation_VBM_Performance}
\end{figure}

\begin{figure}[!p]
    \centering
        \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/ST6_6_P.png}
        \caption{Original VBM Reconstruction}%
        \label{fig:AE4_SVD_error_bar_comparison_pearsonR_absolute}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/ST6_6_GT.png}
        \caption{Original VBM Grount Truth}%
        \label{fig:SAT1-3_GT}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/ST6_2_P.png}
        \caption{Residualised VBM Reconstruction}%
        \label{fig:AE4_SVD_error_bar_comparison_pearsonR_absolute}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/ST6_2_GT.png}
        \caption{Residualised VBM Grount Truth}%
        \label{fig:SAT1-3_GT}
    \end{subfigure}
    \caption[3D Tracts-to-VBM Translations]{{\bf 3D Tracts-to-VBM Translations} for original and residualised data. Red structures represents positive correlated data, while blue represents negative anti-correlated data. Images were hard shrunk to eliminate background noise.}
    \label{fig:VBM_Reconstructions}
\end{figure}

\subsection{Linking Structure to Function}

Encouraged by the previous findings, direct structure-to-function translation was finally attempted using the model in Figure \ref{fig:E2EAE} with \(K=1024\) and \(4\) FMLs on both original and residualised data (Figure \ref{fig:TranslationTraining}). These were accompanied by a similar PLS sensitivity study (Figure \ref{fig:DMN_pls_refinement}). In the case of residualised data, training proved difficult, owing to the sparse nature of the 0-mean data, with both the DL and PLS models achieving similar performances on the training set (Figures \ref{fig:DMN_reg_MSE}-\ref{fig:DMN_reg_P}). Yet, the DL network did prove capable of capturing several individual particularities (Figure \ref{fig:ST3_10r_P}). The original data network struggled in capturing individual particularities, continuing to predict a form of the DMN population mean (Figure \ref{fig:ST4_38_P}). 

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/dmn_e2e_orig_mse_train_curves.png}
        \caption{Original MSE}%
        \label{fig:dmn_e2e_orig_mse_train_curves}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dmn_e2e_orig_pearson_train_curves.png}
        \caption{Original Correlation}%
        \label{fig:dmn_e2e_orig_pearson_train_curves}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dmn_e2e_reg_mse_train_curves.png}
        \caption{Residualised MSE}%
        \label{fig:dmn_e2e_reg_mse_train_curves}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/dmn_e2e_reg_pearson_train_curves}
        \caption{Residualised Correlation}%
        \label{fig:dmn_e2e_reg_pearson_train_curves}
    \end{subfigure}
    \caption[Translation Training Curves]{{\bf Translation Training Curves.} In \textbf{(a)}-\textbf{(b)}, the dashed red line is the average metric value between the DMN population mean and the test set subjects.}
    \label{fig:TranslationTraining}
\end{figure}


\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/pls_dmn_original_sensitivity_mse.png}
        \caption{Original MSE}%
        \label{fig:pls_dmn_original_sensitivity_mse}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_dmn_original_sensitivity_pearson.png}
        \caption{Original Correlation}%
        \label{fig:pls_dmn_original_sensitivity_pearson}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_dmn_regressed_sensitivity_mse.png}
        \caption{Residualised MSE}%
        \label{fig:pls_dmn_regressed_sensitivity_mse}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/pls_dmn_regressed_sensitivity_pearson}
        \caption{Residualised Correlation}%
        \label{fig:pls_dmn_regressed_sensitivity_pearson}
    \end{subfigure}
    \caption[PLS Refinement Study]{{\bf PLS Refinement Study} for various K-top PCA components and across multiple PCA components. The dashed red line represents the performance of the best DL models. The best original data model used 2048 PCA and 11 PLS components, while the best residualised used 4096 PCA and 10 PLS components, suggesting that smaller latent dimensionalities could be attempted for the DL networks.}
    \label{fig:DMN_pls_refinement}
\end{figure}

\begin{figure}[!p]
    \centering
    \begin{subfigure}{0.495\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/DMN_original_MSE.png}
        \caption{Original MSE}%
        \label{fig:DMN_original_MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/DMN_original_MSE.pngPearson R.png}
        \caption{Original Correlation}%
        \label{fig:DMN_original_P}
    \end{subfigure}
    \vskip\baselineskip
    % \quad
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/DMN_regressed_MSE.png}
        \caption{Residualised MSE}%
        \label{fig:DMN_reg_MSE}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.495\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/DMN_regressed_MSE.pngPearson R}
        \caption{Residualised Correlation}%
        \label{fig:DMN_reg_P}
    \end{subfigure}
    \caption[DL vs. PLS Translation Performance]{{\bf DL vs. PLS Translation Performance.} For the residualised dataset, several PLS models were compared, including the ones producing the Best MSE, the best Pearson R (Pr) and one with an identical 1024 latent space.}
    \label{fig:Translation_Performance}
\end{figure}

\begin{figure}[!p]
    \centering
        \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/ST4_38_P.png}
        \caption{Original DMN Reconstruction}%
        \label{fig:ST4_38_P}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/ST4_38_GT.png}
        \caption{Original DMN Grount Truth}%
        \label{fig:ST4_38_GT}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{figures/ST3_10r_P.png}
        \caption{Residualised DMN Reconstruction}%
        \label{fig:ST3_10r_P}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}{0.65\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/ST3_10r_GT.png}
        \caption{Residualised DMN Grount Truth}%
        \label{fig:ST3_10r_GT}
    \end{subfigure}
    \caption[3D Tracts-to-DMN Translations]{{\bf 3D Tracts-to-DMN Translations} for original and residualised data. Red structures represents positive correlated data, while blue represents negative anti-correlated data. Images were hard shrunk to eliminate background noise.}
    \label{fig:AE_Reconstructions}
\end{figure}


